module gitlab.com/golanglab/microservices-go/mssc-brewery

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/rs/xid v1.2.1
	golang.org/x/mod v0.2.0 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/tools v0.0.0-20200128002243-345141a36859 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)

// Copyright 2017 Dmitriy Mirkarimov. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// The text above including the current line will be ignored by godoc

// Package web provides functionality related to application web components such as models etc.
package web

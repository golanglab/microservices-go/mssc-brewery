package controller

import (
	"encoding/json"
	"fmt"
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/services"
	"net/http"
)

type BeerController struct {
	service services.GetBeerByIdFunc
}

var instance *BeerController

func NewBeerController(svc services.GetBeerByIdFunc, pattern string) *BeerController {
	if instance == nil {
		instance = new(BeerController)
		instance.service = svc
		http.HandleFunc(pattern, beerContentHandler)
	}
	return instance
}

func beerContentHandler(w http.ResponseWriter, r *http.Request) {
	//TODO Extract UUID value from the path
	response := instance.service(3876)

	data, err := json.Marshal(response)

	if err != nil {
		panic("Ooops")
	}
	fmt.Fprint(w, string(data))
}

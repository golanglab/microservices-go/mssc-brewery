package controller

import (
	"encoding/json"
	"fmt"
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/model"
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/services"
	"log"
	"net/http"
)

// Customer controller
type CustomerControllerInterface interface {
	GetCustomer() *model.CustomerDto
	GetService() services.CustomerService
	GetHandler() http.Handler
	GetPattern() string
}

type customerController struct {
	service services.CustomerService
	handler http.Handler
	pattern string
}

var customerControllerInstance *customerController

type customerHandler struct{}

var handler http.Handler

func CreateCustomerControllerSingleton(svc services.CustomerService, pattern string) CustomerControllerInterface {

	if customerControllerInstance == nil {
		customerControllerInstance = new(customerController)
		customerControllerInstance.service = svc
		log.Printf("(%p, %T)\n", customerControllerInstance, customerControllerInstance)
		//handler = customerHandler{}
		customerControllerInstance.handler = customerHandler{}
		customerControllerInstance.pattern = pattern
		http.Handle(customerControllerInstance.pattern, customerControllerInstance.handler)

	}
	return customerControllerInstance
}

func (cc *customerController) GetCustomer() *model.CustomerDto {
	return cc.service.GetCustomerByID(8910)
}

func (cc *customerController) GetService() services.CustomerService {
	return cc.service
}

func (cc *customerController) GetHandler() http.Handler {
	return cc.handler
}

func (cc *customerController) GetPattern() string {
	return cc.pattern
}

func (h customerHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//TODO Extract UUID value from the path
	//response := customerControllerInstance.service.GetCustomerByID(6752)

	response := customerControllerInstance.GetCustomer()
	fmt.Printf("(%p, %T)\n", customerControllerInstance, customerControllerInstance)
	fmt.Printf("(%v, %T)\n", customerControllerInstance.GetHandler(), customerControllerInstance.GetHandler())
	/*	fmt.Printf("(%p, %T)\n", customerControllerInstance.GetCustomer, customerControllerInstance.GetCustomer)
		fmt.Printf("(%p, %T)\n", customerControllerInstance.GetService(), customerControllerInstance.GetService())
		fmt.Printf("(%p, %T)\n", customerControllerInstance.service.GetCustomerByID, customerControllerInstance.service.GetCustomerByID)
	*/data, err := json.Marshal(response)

	if err != nil {
		panic("Ooops")
	}
	fmt.Fprint(w, string(data))
}

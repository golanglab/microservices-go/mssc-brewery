package controller

import (
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/services"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetInstance(t *testing.T) {
	var beerController1 *BeerController
	beerController1 = NewBeerController(services.GetBeerByIdFunc(services.GetCoronaById), "/api/v1/new_beer")
	if beerController1 == nil {
		//Test acceptance criteria 1 failed
		t.Error("expected pointer to Singleton after calling NewBeerController(), not nil")
	}

	beer := beerController1.service(1234)
	wantUUID := 1234
	wantName := "Corona"
	if beer.UUID != wantUUID {
		t.Errorf("Beer UUID = %d; want %d", beer.UUID, wantUUID)
	}

	if beer.BeerName != wantName {
		t.Errorf("Beer Name = %s; want %s", beer.BeerName, wantName)
	}

	beerController2 := NewBeerController(services.GetBeerByIdFunc(services.GetMillerById), "/api/v1/new_beer")
	if beerController2 != beerController1 {
		t.Errorf("Expected same instance in beerController2 but it got a different instance")
	}

	beer = beerController2.service(8910)
	wantUUID = 8910
	wantName = "Corona"

	if beer.UUID != wantUUID {
		t.Errorf("Beer UUID = %d; want %d", beer.UUID, wantUUID)
	}

	if beer.BeerName != wantName {
		t.Errorf("Beer Name = %s; want %s", beer.BeerName, wantName)
	}

}

func TestGetBeerByID(t *testing.T) {

	NewBeerController(services.GetBeerByIdFunc(services.GetCoronaById), "/api/v1/new_beer")

	req, err := http.NewRequest("GET", "/spi/v1/new_beer/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(beerContentHandler)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `{"UUID":3876,"BeerName":"Corona","BeerStyle":"Light","Upc":456}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

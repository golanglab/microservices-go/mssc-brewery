package controller

import (
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/services"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCustomerController_GetCustomer(t *testing.T) {
	var svc services.BusinessCustomerService
	customerController1 := CreateCustomerControllerSingleton(svc, "/api/v1/customer")

	if customerController1 == nil {
		t.Error("expected pointer to CustomerControllerInterface after calling CreateCustomerControllerSingleton, not nil")
	}

	expectedController := customerController1
	customerController2 := CreateCustomerControllerSingleton(svc, "/api/v1/customer")

	if customerController2 != expectedController {
		t.Errorf("Expected same instance in customerController2 but it got a different instance")
	}

	got := customerController2.GetCustomer()

	if got.UUID != 8910 {
		t.Errorf("Expected 8910 got %d", got.UUID)
	}
}

func TestGetCustomerByID(t *testing.T) {
	var svc services.BusinessCustomerService
	CreateCustomerControllerSingleton(svc, "/api/v1/customer")

	req, err := http.NewRequest("GET", "/spi/v1/customer/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler = customerHandler{}
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

// Copyright 2017 Dmitriy Mirkarimov. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// The text above including the current line will be ignored by godoc

// Package model implements different entities used in the application such as beer, customer etc
package model

package model

import (
	"fmt"
	"testing"
)

func TestXID(t *testing.T) {
	id := getXid()
	fmt.Printf("github.com/rs/xid:              %s\n", id.String())
	fmt.Printf("github.com/rs/xid:              %T\n", id)
}

func TestUUID(t *testing.T) {
	id := genUUID()
	fmt.Printf("github.com/google/uuid:              %s\n", id.String())
	fmt.Printf("github.com/google/uuid:              %T\n", id)
}

package model

// https://github.com/rs/xid
// https://github.com/google/uuid
// https://godoc.org/github.com/google/uuid
import (
	"github.com/google/uuid"
	"github.com/rs/xid"
)

func getXid() xid.ID {
	id := xid.New()
	return id
}

func genUUID() uuid.UUID {
	id := uuid.New()
	return id
}

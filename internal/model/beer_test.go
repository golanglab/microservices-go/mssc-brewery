package model

import "testing"

func TestBeerDto(t *testing.T) {
	got := &BeerDto{
		BeerName:  "Galaxy Cat",
		BeerStyle: "Pale Ale",
		UUID:      1234,
		Upc:       456,
	}
	if got.UUID != 1234 {
		t.Errorf("Beer UUID = %d; want 1234", got.UUID)
	}
}

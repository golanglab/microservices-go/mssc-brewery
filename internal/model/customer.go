package model

// Customer entity
type CustomerDto struct {
	UUID int
	Name string
}

// Since Go does not support classes, there is no such concept as a constructor. However,
// one conventional idiom you will encounter in Go is the use of a factory function to create and initialize
// values for a type
// Examples:
//
//     // Field is ignored.
//    customer := model.NewCustomerDto(1234, "Adam Smith")
//
func NewCustomerDto(uuid int, name string) *CustomerDto {
	return &CustomerDto{uuid, name}
}

// TODO Implement equalTo method

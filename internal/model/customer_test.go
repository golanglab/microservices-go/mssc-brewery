package model

import "testing"

func TestCustomerDto(t *testing.T) {
	got := NewCustomerDto(1234, "Adam Smith")

	if got.UUID != 1234 {
		t.Errorf("Customer UUID = %d; want 1234", got.UUID)
	}
}

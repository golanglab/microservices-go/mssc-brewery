package model

// Beer is the main entity the application is supposed to manage.
//
// Example:
//  // Any field can be actually empty at this point.
//
//  beer := &model.BeerDto{
//  BeerName:  "Galaxy Cat",
//  BeerStyle: "Pale Ale",
//  UUID:      1234,
//  Upc:       456,
//  }
type BeerDto struct {
	UUID                int
	BeerName, BeerStyle string
	Upc                 int
}

// TODO Implement equalTo method

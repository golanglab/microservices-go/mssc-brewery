package services

import "testing"

func TestCustomerService_GetCustomerByID(t *testing.T) {
	var bcs BusinessCustomerService
	got := bcs.GetCustomerByID(456)

	if got.UUID != 456 {
		t.Errorf("Expected 456 got %d", got.UUID)
	}

	if got.Name != "Business Customer Service" {
		t.Errorf("Expected 'Customer Service' got %s", got.Name)
	}

	var css PrivateCustomerService
	got = css.GetCustomerByID(789)

	if got.UUID != 789 {
		t.Errorf("Expected 789 got %d", got.UUID)
	}

	if got.Name != "Private Customer Service" {
		t.Errorf("Expected 'Private Customer Service' got %s", got.Name)
	}

}

package services

import "testing"

func TestGetCoronaById(t *testing.T) {
	wantName := "Corona"
	wantUUID := 8910
	wantStyle := "Light"
	wantUpc := 456
	got := GetCoronaById(8910)

	if got.UUID != wantUUID {
		t.Errorf("Beer UUID = %d; want %d", got.UUID, wantUUID)
	}

	if got.BeerName != wantName {
		t.Errorf("Beer Name = %s; want %s", got.BeerName, wantName)
	}

	if got.BeerStyle != wantStyle {
		t.Errorf("Beer Style = %s; want %s", got.BeerStyle, wantStyle)
	}

	if got.Upc != wantUpc {
		t.Errorf("Beer Upc = %d; want %d", got.Upc, wantUpc)
	}

}

func TestGetMillerById(t *testing.T) {
	wantName := "Miller"
	wantUUID := 8910
	wantStyle := "Light"
	wantUpc := 789
	got := GetMillerById(8910)

	if got.UUID != wantUUID {
		t.Errorf("Beer UUID = %d; want %d", got.UUID, wantUUID)
	}

	if got.BeerName != wantName {
		t.Errorf("Beer Name = %s; want %s", got.BeerName, wantName)
	}

	if got.BeerStyle != wantStyle {
		t.Errorf("Beer Style = %s; want %s", got.BeerStyle, wantStyle)
	}

	if got.Upc != wantUpc {
		t.Errorf("Beer Upc = %d; want %d", got.Upc, wantUpc)
	}

}

func TestGetBeerByIdFunc_GetBeerById(t *testing.T) {
	wantName := "Corona"
	wantUUID := 8910
	wantStyle := "Light"
	wantUpc := 456

	got := GetBeerByIdFunc(GetCoronaById)(wantUUID)

	if got.UUID != wantUUID {
		t.Errorf("Beer UUID = %d; want %d", got.UUID, wantUUID)
	}

	if got.BeerName != wantName {
		t.Errorf("Beer Name = %s; want %s", got.BeerName, wantName)
	}

	if got.BeerStyle != wantStyle {
		t.Errorf("Beer Style = %s; want %s", got.BeerStyle, wantStyle)
	}

	if got.Upc != wantUpc {
		t.Errorf("Beer Upc = %d; want %d", got.Upc, wantUpc)
	}

	wantName = "Miller"
	wantUUID = 8910
	wantStyle = "Light"
	wantUpc = 789
	got = GetBeerByIdFunc(GetMillerById)(wantUUID)

	if got.UUID != wantUUID {
		t.Errorf("Beer UUID = %d; want %d", got.UUID, wantUUID)
	}

	if got.BeerName != wantName {
		t.Errorf("Beer Name = %s; want %s", got.BeerName, wantName)
	}

	if got.BeerStyle != wantStyle {
		t.Errorf("Beer Style = %s; want %s", got.BeerStyle, wantStyle)
	}

	if got.Upc != wantUpc {
		t.Errorf("Beer Upc = %d; want %d", got.Upc, wantUpc)
	}

}

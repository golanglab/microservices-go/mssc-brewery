package services

import "gitlab.com/golanglab/microservices-go/mssc-brewery/internal/model"

// Sample implementation of CustomerService interface
type BusinessCustomerService struct {
}

// Creates CustomerDto with UUID field set passed parameter and Name field set to "Customer Service Trivial"
func (csvc BusinessCustomerService) GetCustomerByID(UUID int) *model.CustomerDto {
	return model.NewCustomerDto(UUID, "Business Customer Service")
}

package services

import "gitlab.com/golanglab/microservices-go/mssc-brewery/internal/model"

// Sample implementation of CustomerService interface
type PrivateCustomerService struct {
}

// Creates CustomerDto with UUID field set passed parameter and Name field set to "Customer Service Simple"
// Note that there is no receiver's  name defined but only its type. It can be done since
// the receiver name is not being used inside function body.
// Read https://stackoverflow.com/questions/40950877/is-unnamed-arguments-a-thing-in-go
func (PrivateCustomerService) GetCustomerByID(UUID int) *model.CustomerDto {
	return model.NewCustomerDto(UUID, "Private Customer Service")
}

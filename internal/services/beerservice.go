package services

import "gitlab.com/golanglab/microservices-go/mssc-brewery/internal/model"

// Used the idea from https://stackoverflow.com/questions/20728965/golang-function-pointer-as-a-part-of-a-struct
type GetBeerByIdFunc func(uuid int) *model.BeerDto

func (b GetBeerByIdFunc) GetBeerById(uuid int) *model.BeerDto {
	return b(uuid)
}

func GetCoronaById(uuid int) *model.BeerDto {
	return &model.BeerDto{
		BeerName:  "Corona",
		BeerStyle: "Light",
		UUID:      uuid,
		Upc:       456,
	}
}

func GetMillerById(uuid int) *model.BeerDto {
	return &model.BeerDto{
		BeerName:  "Miller",
		BeerStyle: "Light",
		UUID:      uuid,
		Upc:       789,
	}
}

package services

import "gitlab.com/golanglab/microservices-go/mssc-brewery/internal/model"

// Interface to be implemented by different services with different ways
// to obtain customer data
type CustomerService interface {
	GetCustomerByID(UUID int) *model.CustomerDto
}

// Currently this is a simple command line based application.
package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/controller"
	"gitlab.com/golanglab/microservices-go/mssc-brewery/internal/services"
	"log"
	"net/http"
	"reflect"
	"strconv"

	//"net/url"
	"strings"
)

type coronaHandler struct{}

var handler http.Handler

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	port := 8082
	var businessCustomerService services.BusinessCustomerService
	// var privateCustomerService services.PrivateCustomerService

	businessController := controller.CreateCustomerControllerSingleton(businessCustomerService, "/api/v1/customer/business/")
	log.Printf("(%p, %T,%v)\n", businessController, businessController, businessController)
	log.Printf("(%p, %T)\n", businessController.GetService(), businessController.GetService())

	// Technically I could have used just
	// controller.CreateCustomerControllerSingleton(businessCustomerService, "/api/v1/customer/business/")

	/*
		Since I used a singleton pattern, there is no any sense to make a call below.
		privateController will get the same value as previously created businessController

		privateController := controller.CreateCustomerControllerSingleton(privateCustomerService, "/api/v1/customer/private/")
		log.Printf("(%p, %T)\n", privateController, privateController)
		log.Printf("(%p, %T)\n", privateController.GetService(), privateController.GetService())
	*/
	controller.NewBeerController(services.GetBeerByIdFunc(services.GetMillerById), "/api/v1/miller_beer/")

	handler = coronaHandler{}
	http.Handle("/api/v1/corona_beer/", handler)

	http.HandleFunc("/", indexHandler)

	var tmp http.Handler = http.HandlerFunc(tmpHandler)
	fmt.Printf("(%v, %T)\n", tmp, tmp) // Output: (0x654240, http.HandlerFunc)
	http.Handle("/tmp1/", tmp)
	http.HandleFunc("/tmp2/", tmpHandler)

	log.Printf("Server starting on port %v\n", port)

	// Get a list of routes and params in Golang HTTP or Gorilla package
	//https://stackoverflow.com/questions/37228632/get-a-list-of-routes-and-params-in-golang-http-or-gorilla-package
	v := reflect.ValueOf(http.DefaultServeMux).Elem()
	fmt.Printf("routes: %v\n", v.FieldByName("m"))

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil))

}

func tmpHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to Go lang Web App.")
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to Brewery Web App.")
}
func (h coronaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//response := services.GetCoronaById(8910)
	response := services.GetBeerByIdFunc(services.GetCoronaById)(8910)
	data, err := json.Marshal(response)

	if err != nil {
		panic("Ooops")
	}

	log.Printf("Loogin handler %s\n", r.URL)
	log.Printf("Loogin handler %s\n", r.URL.RequestURI())
	log.Printf("%q\n", strings.SplitAfterN(r.URL.RequestURI(), "/", 5))
	s := strings.SplitAfterN(r.URL.RequestURI(), "/", 5)
	log.Printf("Loogin handler %s\n", s[4])
	i, err := strconv.Atoi(s[4])

	if err != nil {
		log.Printf("Error: %s", err)
		http.Error(w, err.Error(), 500)
	} else {
		log.Printf("Loogin handler %d\n", i)
		fmt.Fprint(w, string(data))
	}

}
